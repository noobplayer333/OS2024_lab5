<font size =6>**操作系统原理 实验五**</font>

## 个人信息

【院系】计算机学院

【专业】计算机科学与技术

【学号】22336288

【姓名】喻梓轩

## 实验题目

内核线程

## 实验目的

1. 学习C语言的可变参数机制的实现方法，实现可变参数机制，以及实现一个较为简单的printf函数。
2. 实现内核线程的实现。
3. 重点理解`asm_switch_thread`是如何实现线程切换的，体会操作系统实现并发执行的原理。
4. 实现基于时钟中断的时间片轮转(RR)调度算法，并实现一个线程调度算法。

## 实验要求

1. 实现了可变参数机制及printf函数。
2. 自行实现PCB，实现线程。
3. 了解线程调度，并实现一个调度算法。
4. 撰写实验报告。

## 实验方案

本次实验包括四个部分。

Assignment 1通过可变参数机制实现printf。

Assignment 2设计PCB，并根据PCB来实现线程。

Assignment 3编写线程相关函数，例如线程切换等。

Assignment 4实现调度算法。

## 实验过程

#### Assignment 1

该部分实现printf。

首先回顾一下可变参数机制。在C语言中，可变参数在固定参数的后一部分，用 ... 表示。例如:

```c
int printf(const char* const _Format, ...);
```

如何引用可变参数列表中的参数呢？<stdarg.h>中定义了一个变量类型和三个宏:

```c
va_list; #指向可变参数列表的指针
void va_start(va_list ap,last_arg); #初始化可变参数列表指针，即指向last_arg的下一个参数
type va_arg(va_list ap,type); #以类型type返回可变参数，并使ap指向下一个参数
void va_end(va_list ap); #清零ap
```

自己实现的“stdarg.h"可以按如下方式定义:

```c
typdef char * va_list;
#define _INTSIZEOF(n) ( (sizeof(n)+sizeof(int)-1) & ~(sizeof(int)-1) )
#define va_start(ap,v) ( ap = (va_list)&v + _INTSIZEOF(v) )
#define va_arg(ap, type) ( *(type *)((ap += _INTSIZEOF(type)) - _INTSIZEOF(type)))
#define va_end(ap) ( ap = (va_list)0 )
```

接着实现标准输入输出库,具体实现方式是实现一个STDIO类:

```c
STDIO::STDIO()
{
    initialize();
}
#初始化，将屏幕显存基地址存入screen变量
void STDIO::initialize()
{
    screen = (uint8 *)0xb8000;
}
#在x行y列打印字符c，此时字符c是用8位二进制数存储，颜色被设置为color
void STDIO::print(uint x, uint y, uint8 c, uint8 color)
{

    if (x >= 25 || y >= 80)
    {
        return;
    }

    uint pos = x * 80 + y;
    screen[2 * pos] = c;
    screen[2 * pos + 1] = color;
}
#在当前光标位置打印字符c，颜色为color
void STDIO::print(uint8 c, uint8 color)
{
    uint cursor = getCursor();
    screen[2 * cursor] = c;
    screen[2 * cursor + 1] = color;
    cursor++;
    if (cursor == 25 * 80)
    {
        rollUp();
        cursor = 24 * 80;
    }
    moveCursor(cursor);
}
#在当前光标位置打印字符c，颜色白色
void STDIO::print(uint8 c)
{
    print(c, 0x07);
}
#移动光标到position处
void STDIO::moveCursor(uint position)
{
    if (position >= 80 * 25)
    {
        return;
    }

    uint8 temp;

    // 处理高8位
    temp = (position >> 8) & 0xff;
    asm_out_port(0x3d4, 0x0e);
    asm_out_port(0x3d5, temp);

    // 处理低8位
    temp = position & 0xff;
    asm_out_port(0x3d4, 0x0f);
    asm_out_port(0x3d5, temp);
}
#获取当前光标位置
uint STDIO::getCursor()
{
    uint pos;
    uint8 temp;

    pos = 0;
    temp = 0;
    // 处理高8位
    asm_out_port(0x3d4, 0x0e);
    asm_in_port(0x3d5, &temp);
    pos = ((uint)temp) << 8;

    // 处理低8位
    asm_out_port(0x3d4, 0x0f);
    asm_in_port(0x3d5, &temp);
    pos = pos | ((uint)temp);

    return pos;
}
#移动光标到x行y列
void STDIO::moveCursor(uint x, uint y)
{
    if (x >= 25 || y >= 80)
    {
        return;
    }

    moveCursor(x * 80 + y);
}
#向上滚屏
void STDIO::rollUp()
{
    uint length;
    length = 25 * 80;
    for (uint i = 80; i < length; ++i)
    {
        screen[2 * (i - 80)] = screen[2 * i];
        screen[2 * (i - 80) + 1] = screen[2 * i + 1];
    }

    for (uint i = 24 * 80; i < length; ++i)
    {
        screen[2 * i] = ' ';
        screen[2 * i + 1] = 0x07;
    }
}
#打印字符串str
int STDIO::print(const char *const str)
{
    int i = 0;

    for (i = 0; str[i]; ++i)
    {
        switch (str[i])
        {
        case '\n':
            uint row;
            row = getCursor() / 80;
            if (row == 24)
            {
                rollUp();
            }
            else
            {
                ++row;
            }
            moveCursor(row * 80);
            break;

        default:
            print(str[i]);
            break;
        }
    }

    return i;
}

```

这里多次重载了STDIO::print(), 方便我们后面实现稀奇古怪功能。

为了格式化输出，我们当然需要一个类似缓冲区的东西存放要输出的内容，比如buffer。printf收到一串字符串和一堆参数，处理后放到buffer里面：

```c
int printf_add_to_buffer(char *buffer, char c, int &idx, const int BUF_LEN)
{
    int counter = 0;

    buffer[idx] = c;
    ++idx;

    if (idx == BUF_LEN)
    {
        buffer[idx] = '\0';
        counter = stdio.print(buffer);
        idx = 0;
    }

    return counter;
}
```

其中各参数: buffer,c,idx,BUF_LEN 分别表示buffer基地址，放入的字符，buffer当前位置索引以及buffer容量。

最后开始写真正的printf, 实际上教程里已经实现了一个简单的printf，我们要做的只是随意扩建一些:

首先实现8进制数的输出，实际上逻辑和16进制一样，于是把他们的分支写在一起:

```c
int printf(const char *const fmt, ...)
{
    const int BUF_LEN = 32;

    char buffer[BUF_LEN + 1];
    char number[33];

    int idx, counter;
    va_list ap;

    va_start(ap, fmt);
    idx = 0;
    counter = 0;

    for (int i = 0; fmt[i]; ++i)
    {
        if (fmt[i] != '%')
        {
            counter += printf_add_to_buffer(buffer, fmt[i], idx, BUF_LEN);
        }
        else
        {
            i++;
            if (fmt[i] == '\0')
            {
                break;
            }
            

            switch (fmt[i])
            {
            case '%':
                counter += printf_add_to_buffer(buffer, fmt[i], idx, BUF_LEN);
                break;

            case 'c':
                counter += printf_add_to_buffer(buffer, va_arg(ap, char), idx, BUF_LEN);
                break;

            case 's':
                buffer[idx] = '\0';
                idx = 0;
                counter += stdio.print(buffer);
                counter += stdio.print(va_arg(ap, const char *));
                break;

            
            case 'd':
            case 'o':
            case 'x':
                int temp = va_arg(ap, int);

                if (temp < 0 && fmt[i] == 'd')
                {
                    counter += printf_add_to_buffer(buffer, '-', idx, BUF_LEN);
                    temp = -temp;
                }

                itos(number, temp, (fmt[i] == 'd' ? 10 : (fmt[i]=='x'?16:8)));
                for (int j = 0; number[j]; ++j)
                {
                    counter += printf_add_to_buffer(buffer, number[j], idx, BUF_LEN);
                }
                break;
            }
        }
    }
    buffer[idx] = '\0';
    counter += stdio.print(buffer);

    return counter;
}
```

接着实现%p，也就是输出对应变量的地址。

实现起来很简单，就是把对应的temp赋值为*va_arg(ap,int)... :

```c
case 'p':
                ptemp = (unsigned int)(va_arg(ap,int));
                itos(number,ptemp,16);
                counter += printf_add_to_buffer(buffer,'0',idx,BUF_LEN);
                counter += printf_add_to_buffer(buffer,'x',idx,BUF_LEN);
                for(int j = 0; number[j]; ++j){
                    counter += printf_add_to_buffer(buffer,number[j],idx,BUF_LEN);
                }
                break;
```

接着让我们实现浮点数%f。

主要思想就是把整数部分和小数部分分离，关于浮点数精度问题，只进行一次浮点数运算并向上取整可以有效解决此问题，代码如下：

```c
case 'f':
                ftemp = va_arg(ap, double);
                if (ftemp < 0)
                {
                    counter += printf_add_to_buffer(buffer, '-', idx, BUF_LEN);
                    ftemp = -ftemp;
                }
                temp = (int)ftemp;
                ftemp -= (double)temp;
                itos(number,temp,10);
                for(int j = 0;number[j];j++){
                    counter += printf_add_to_buffer(buffer,number[j],idx,BUF_LEN);
                }
                counter += printf_add_to_buffer(buffer,'.',idx,BUF_LEN);

                temp = (int)(ftemp * 1000000.0 +0.5f);
                itos(number,temp,10);
                for(int j = 0;number[j];j++){
                    counter += printf_add_to_buffer(buffer,number[j],idx,BUF_LEN);
                }             
                break;
```

标准库里的printf大差不差就这些功能，精度控制啥的懒得搞了，顺便还修改了一下类函数代码，让其可以控制输出字符的颜色。

最终整体效果如下：

![image-20240519155807811](/home/yzx/.config/Typora/typora-user-images/image-20240519155807811.png)

#### Assignment 2

本部分实现并完善PCB的功能。

先回顾线程相关知识：

创建的线程状态有5个，分别是创建态，运行态，就绪态，阻塞态和终止态，用枚举类型实现如下：

```c
enum ProgramStatus
{
    CREATED,
    RUNNING,
    READY,
    BLOCKED,
    DEAD
};
```

线程的组成部分线程各自的栈，状态，优先级，运行时间，线程负责运行的函数，函数的参数等，这些组成部分被集中保存在一个结构中——PCB(Process Control Block)，如下所示：

```c
struct PCB
{
    int *stack;                      // 栈指针，用于调度时保存esp
    char name[MAX_PROGRAM_NAME + 1]; // 线程名
    enum ProgramStatus status;       // 线程的状态
    int priority;                    // 线程优先级
    int pid;                         // 线程pid
    int ticks;                       // 线程时间片总时间
    int ticksPassedBy;               // 线程已执行时间
    ListItem tagInGeneralList;       // 线程队列标识
    ListItem tagInAllList;           // 线程队列标识
};
```

创建线程时，我们向内存申请一个PCB。我们将一个PCB的大小设置为4096字节，也就是一个页的大小。由于没有实现基于二级分页机制的内存管理，为了解决PCB的内存分配问题，我们在内存中预留若干PCB内存空间。

```c
// PCB的大小，4KB。
const int PCB_SIZE = 4096;         
// 存放PCB的数组，预留了MAX_PROGRAM_AMOUNT个PCB的大小空间。
char PCB_SET[PCB_SIZE * MAX_PROGRAM_AMOUNT]; 
// PCB的分配状态，true表示已经分配，false表示未分配。
bool PCB_SET_STATUS[MAX_PROGRAM_AMOUNT];     
```

完成以上内容后，接着实现PCB的分配与释放：

```c
PCB *ProgramManager::allocatePCB()
{
    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        if (!PCB_SET_STATUS[i])
        {
            PCB_SET_STATUS[i] = true;
            return (PCB *)((int)PCB_SET + PCB_SIZE * i);
        }
    }

    return nullptr;
}

void ProgramManager::releasePCB(PCB *program)
{
    int index = ((int)program - (int)PCB_SET) / PCB_SIZE;
    PCB_SET_STATUS[index] = false;
}
```

每次创建线程时都要分配一个PCB:

```c
int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    thread->ticks = priority * 10;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;

    // 线程栈
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    thread->stack[4] = (int)function;
    thread->stack[5] = (int)program_exit;
    thread->stack[6] = (int)parameter;

    allPrograms.push_back(&(thread->tagInAllList));
    readyPrograms.push_back(&(thread->tagInGeneralList));

    // 恢复中断
    interruptManager.setInterruptStatus(status);

    return thread->pid;
}
```

我们创建一个进程，然后用Assignment 1中实现的printf输出PCB各参数:

```c
printf("stack:%p\n",firstThread->stack);
printf("name:%s\n",firstThread->name);
printf("status:%d\n",firstThread->status);    
printf("priority:%d\n",firstThread->priority);
printf("pid:%d\n",firstThread->pid);
printf("ticks:%d\n",firstThread->ticks);
printf("ticksPassedBy:%d\n",firstThread->ticksPassedBy);
```

结果如下:

![image-20240519163316415](/home/yzx/.config/Typora/typora-user-images/image-20240519163316415.png)

#### Assignment 3

gdb调试，在每次c_time_interrupt_handler()前设置断点，并用p cur->ticks查看ticks，发现每次时钟中断使得ticks -1,符合预期。

![image-20240521195000473](/home/yzx/.config/Typora/typora-user-images/image-20240521195000473.png)

每次ticks归零时，需要更换线程，在Thread 1归零前查看寄存器信息如下：

![image-20240521200422029](/home/yzx/.config/Typora/typora-user-images/image-20240521200422029.png)

切换到thread 2 时，如下：

![image-20240521201206246](/home/yzx/.config/Typora/typora-user-images/image-20240521201206246.png)

可以看到栈地址发生更改，而当我们重新回到Thread1时参数被恢复。

#### Assignment 4

本节实现一个调度算法。

实现先到先服务。

这很简单，就是完全忽略时钟中断，一直运行进程直到进程结束，详情放在代码里了。

```c
void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    if (readyPrograms.size() == 0)
    {
        interruptManager.setInterruptStatus(status);
        return;
    }

    if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::READY;
        running->ticks = running->priority * 10;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }

    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    running = next;
    readyPrograms.pop_front();
    asm_switch_thread(cur, next);

    interruptManager.setInterruptStatus(status);
}
```



## 实验总结

debug过于难受了。

在本次实验中，我们深入学习了C语言的可变参数机制的实现方法。这不仅包括对可变参数的使用，还揭示了其背后的工作原理。在掌握了这些原理后，我们亲自实现了一个简化版的printf函数。通过实现这个函数，我们更深入地理解了可变参数的应用和灵活性。此外，我们利用printf和gdb工具进行调试，进一步巩固了我们对C语言调试技巧的掌握。

另一个重要的实验内容是内核线程的实现。我们首先定义了线程控制块（PCB）的数据结构，并创建了PCB，填入线程执行所需的各种参数。在此基础上，我们实现了基于时钟中断的时间片轮转（RR）调度算法以及先到先服务算法。这一部分的重点是理解asm_switch_thread函数是如何实现线程切换的。在实现和调试的过程中，我们体会到了操作系统并发执行的基本原理。

## 参考文献

（如有要列出，包括网上资源）
#include "asm_utils.h"
#include "interrupt.h"
#include "stdio.h"

// 屏幕IO处理器
STDIO stdio;
// 中断管理器
InterruptManager interruptManager;


extern "C" void setup_kernel()
{
    // 中断处理部件
    interruptManager.initialize();
    // 屏幕IO处理部件
    stdio.initialize();
    interruptManager.enableTimeInterrupt();
    interruptManager.setTimeInterrupt((void *)asm_time_interrupt_handler);
    //asm_enable_interrupt();
    int a = 10;
    int b = 5;
    int *p = &a;
    int *q = &b;
    printf("print percentage: %%\n"
           "print char \"N\": %c\n"
           "print string \"Hello World!\": %s\n"
           "print decimal: \"-1234\": %d\n"
           "print hexadecimal \"0x7abcdef0\": %x\n"
           "print octal \"0777\": %o\n"
           "print pointer \"p\" : %p\n"
           "print float/double \"114.514\": %f\n",
           'N', "Hello World!", -1234, 0x7abcdef0, 0777, p, 114.514);
    printf("%R Red\n");
    printf("%G Green\n");
    printf("%B Blue\n");
    //uint a = 1 / 0;
    asm_halt();
}
